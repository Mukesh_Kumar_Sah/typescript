var { SpecReporter } = require('jasmine-spec-reporter');

var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');
var jasmineReporters = require('jasmine-reporters');
var reportsDirectory = './reports';
var dashboardReportDirectory = reportsDirectory + '/dashboardReport';

exports.config = {
    framework: 'jasmine', //Type of Framework used 
    directConnect: true, //no need to run 'webdriver-manager start'
    specs: ['./home-spec.ts'],//Name of the Specfile 
    restartBrowserBetweenTests: false,//restart before each test

    onPrepare() {
        require('ts-node').register({
            project: require('path').join(__dirname, '../tsconfig.json') // Relative path of tsconfig.json file 
        });

        //for console output
        jasmine.getEnv().addReporter(new SpecReporter({
            displayStacktrace: 'all',      // display stacktrace for each failed assertion, values: (all|specs|summary|none) 
            displaySuccessesSummary: false, // display summary of all successes after execution 
            displayFailuresSummary: true,   // display summary of all failures after execution 
            displayPendingSummary: true,    // display summary of all pending specs after execution 
            displaySuccessfulSpec: true,    // display each successful spec 
            displayFailedSpec: true,        // display each failed spec 
            displayPendingSpec: false,      // display each pending spec 
            displaySpecDuration: false,     // display each spec duration 
            displaySuiteNumber: false,      // display each suite number (hierarchical) 
            colors: {
                success: 'green',
                failure: 'red',
                pending: 'yellow'
            },
            prefixes: {
                success: '✓ ',
                failure: '✗ ',
                pending: '* '
            },
            customProcessors: []
        }));

        try {
            var fs = require('fs-extra');
            var date = new Date();
            // var folder = date.toString();
            // var temp= folder.replace(':','-');
            // fs.moveSync(reportsDirectory,'./old-reports/'+temp.toString());
            fs.moveSync(reportsDirectory, './old-reports/' + date.getFullYear() + "-" +
                String(Number(date.getMonth()) + 1) + "-" + date.getDate() + "-" + date.getHours() + "-" + date.getMinutes());
            if (!fs.existsSync(dashboardReportDirectory)) {
                fs.mkdirSync(reportsDirectory);
                fs.mkdirSync(dashboardReportDirectory);
            }
        } catch (err) {
            console.error(err)
        }
        //.xml report
        jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
            consolidateAll: true,
            savePath: reportsDirectory + '/xml',
            filePrefix: 'xmlOutput',
        }));




        jasmine.getEnv().addReporter({
            specDone: function (result) {
                if (result.status == 'failed') {
                    browser.getCapabilities().then(function (caps) {
                        var browserName = caps.get('browserName');

                        browser.takeScreenshot().then(function (png) {
                            var stream = fs.createWriteStream(dashboardReportDirectory + '/' + browserName + '-' + result.fullName + '.png');
                            stream.write(new Buffer(png, 'base64'));
                            stream.end();
                        });
                    });
                }
            }
        });

    },

    onComplete: function () {
        var browserName, browserVersion;
        var capsPromise = browser.getCapabilities();
        capsPromise.then(function (caps) {
            browserName = caps.get('browserName');
            browserVersion = caps.get('version');
            platform = caps.get('platform');
            var HTMLReport = require('protractor-html-reporter-2');
            testConfig = {
                reportTitle: 'Protractor Test Execution Report',
                outputPath: dashboardReportDirectory,
                outputFilename: 'index',
                screenshotPath: './',
                testBrowser: browserName,
                browserVersion: browserVersion,
                modifiedSuiteName: false,
                screenshotsOnlyOnFailure: true,
                testPlatform: platform,

            };

            new HTMLReport().from(reportsDirectory + '/xml/xmlOutput.xml', testConfig);
        });
    },
}