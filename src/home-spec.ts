import { browser, element, by } from "protractor";
describe('Suite 1', function() {
    
    beforeEach(function() { //beforeEach function
		console.log('Before Each Called');
    });
    
	it('Sample Test Case 1', async function() {
        console.log('First it block');
        browser.ignoreSynchronization=true;
        await browser.manage().window().maximize();
        await browser.get('https://google.com/');
        await element(by.name('q')).click();
        await element(by.name('q')).sendKeys('Test');
        await element(by.name('q')).getAttribute("title").then(function(name){
            expect(name).toBe("Search") 
            console.log("Page Title: "+name)}); 
      });
     
      it('Sample Test Case 2', async function() {
        console.log('First it block');
        browser.ignoreSynchronization=true;
        await browser.manage().window().maximize();
        await browser.get('https://google.com/');
        await element(by.name('q')).click();
        await element(by.name('q')).sendKeys('Test');
        await element(by.name('q')).getAttribute("title").then(function(name){
            expect(name).toBe("Search") 
            console.log("Page Title: "+name)}); 
      });
      
      it('Sample Test Case 3', async function() {
        console.log('First it block');
        browser.ignoreSynchronization=true;
        await browser.manage().window().maximize();
        await browser.get('https://google.com/');
        await element(by.name('q')).click();
        await element(by.name('q')).sendKeys('Test');
        await element(by.name('q')).getAttribute("title").then(function(name){
            expect(name).toBe("Search") 
            console.log("Page Title: "+name)}); 
      });
      
      it('Sample Test Case 4', async function() {
        console.log('First it block');
        browser.ignoreSynchronization=true;
        await browser.manage().window().maximize();
        await browser.get('https://google.com/');
        await element(by.name('q')).click();
        await element(by.name('q')).sendKeys('Test');
        await element(by.name('q')).getAttribute("title").then(function(name){
            expect(name).toBe("Search") 
            console.log("Page Title: "+name)}); 
	    });

	afterEach( function(){
		console.log('After Each Called');
	});
});


describe('Suite 2', function() {
    
  beforeEach(function() { //beforeEach function
  console.log('Before Each Called');
  });
  
it('Sample Test Case 1', async function() {
      console.log('First it block');
      browser.ignoreSynchronization=true;
      await browser.manage().window().maximize();
      await browser.get('https://google.com/');
      await element(by.name('q')).click();
      await element(by.name('q')).sendKeys('Test');
      await element(by.name('q')).getAttribute("title").then(function(name){
          expect(name).toBe("Search") 
          console.log("Page Title: "+name)}); 
    });
   
    it('Sample Test Case 2', async function() {
      console.log('First it block');
      browser.ignoreSynchronization=true;
      await browser.manage().window().maximize();
      await browser.get('https://google.com/');
      await element(by.name('q')).click();
      await element(by.name('q')).sendKeys('Test');
      await element(by.name('q')).getAttribute("title").then(function(name){
          expect(name).toBe("Search") 
          console.log("Page Title: "+name)}); 
    });
    
    it('Sample Test Case 3', async function() {
      console.log('First it block');
      browser.ignoreSynchronization=true;
      await browser.manage().window().maximize();
      await browser.get('https://google.com/');
      await element(by.name('q')).click();
      await element(by.name('q')).sendKeys('Test');
      await element(by.name('q')).getAttribute("title").then(function(name){
          expect(name).toBe("Search") 
          console.log("Page Title: "+name)}); 
    });
    
    it('Sample Test Case 4', async function() {
      console.log('First it block');
      browser.ignoreSynchronization=true;
      await browser.manage().window().maximize();
      await browser.get('https://google.com/');
      await element(by.name('q')).click();
      await element(by.name('q')).sendKeys('Test');
      await element(by.name('q')).getAttribute("title").then(function(name){
          expect(name).toBe("Search") 
          console.log("Page Title: "+name)}); 
    });

afterEach( function(){
  console.log('After Each Called');
});
});

describe('Suite 3', function() {
    
  beforeEach(function() { //beforeEach function
  console.log('Before Each Called');
  });
  
it('Sample Test Case 1', async function() {
      console.log('First it block');
      browser.ignoreSynchronization=true;
      await browser.manage().window().maximize();
      await browser.get('https://google.com/');
      await element(by.name('q')).click();
      await element(by.name('q')).sendKeys('Test');
      await element(by.name('q')).getAttribute("title").then(function(name){
          expect(name).toBe("Search") 
          console.log("Page Title: "+name)}); 
    });
   
    it('Sample Test Case 2', async function() {
      console.log('First it block');
      browser.ignoreSynchronization=true;
      await browser.manage().window().maximize();
      await browser.get('https://google.com/');
      await element(by.name('q')).click();
      await element(by.name('q')).sendKeys('Test');
      await element(by.name('q')).getAttribute("title").then(function(name){
          expect(name).toBe("Search") 
          console.log("Page Title: "+name)}); 
    });
    
    it('Sample Test Case 3', async function() {
      console.log('First it block');
      browser.ignoreSynchronization=true;
      await browser.manage().window().maximize();
      await browser.get('https://google.com/');
      await element(by.name('q')).click();
      await element(by.name('q')).sendKeys('Test');
      await element(by.name('q')).getAttribute("title").then(function(name){
          expect(name).toBe("Search") 
          console.log("Page Title: "+name)}); 
    });
    
    it('Sample Test Case 4', async function() {
      console.log('First it block');
      browser.ignoreSynchronization=true;
      await browser.manage().window().maximize();
      await browser.get('https://google.com/');
      await element(by.name('q')).click();
      await element(by.name('q')).sendKeys('Test');
      await element(by.name('q')).getAttribute("title").then(function(name){
          expect(name).toBe("Search") 
          console.log("Page Title: "+name)}); 
    });

afterEach( function(){
  console.log('%cAfter Each Called', 'color: green; font-weight: bold;');
});
});